from django.urls import include, path
from rest_framework import routers
from rest_framework.urlpatterns import format_suffix_patterns

from .apis import InstallmentsUpdateOperationAPI

router = routers.SimpleRouter()
router.register(r"daily-sync", InstallmentsUpdateOperationAPI, "installment-daily-sync")


urlpatterns = [
    path("", include(router.urls)),
]

urlpatterns = format_suffix_patterns(urlpatterns, allowed=["json"])
