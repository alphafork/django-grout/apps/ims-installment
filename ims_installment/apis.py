from ims_operation.apis import OperationBaseAPI

from .serializers import InstallmentsUpdateOperationSerializer


class InstallmentsUpdateOperationAPI(OperationBaseAPI):
    operation = "installment_updation"
    auth_groups = ["Admin", "Accountant"]
    serializer_class = InstallmentsUpdateOperationSerializer
