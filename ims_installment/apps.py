from django.apps import AppConfig


class IMSInstallmentConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "ims_installment"

    model_strings = {
        "INSTALLMENT_SUBSCRIPTION": "InstallmentSubscription",
        "INSTALLMENT_SCHEME": "InstallmentScheme",
        "INSTALLMENT": "Installment",
        "INSTALLMENT_WALLET": "InstallmentWallet",
        "INSTALLMENT_DUE_EXTENSION": "InstallmentDueExtension",
        "INSTALLMENT_DOWN_PAYMENT": "InstallmentDownPayment",
        "INSTALLMENT_TRANSACTION": "InstallmentTransaction",
    }

    operation_apis = ["InstallmentsUpdateOperationAPI"]

    def ready(self):
        import ims_installment.receivers
