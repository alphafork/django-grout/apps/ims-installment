from django.db.models.signals import post_save
from django.dispatch import receiver
from reusable_models import get_model_from_string

from .models import (
    InstallmentDownPayment,
    InstallmentDueExtension,
    InstallmentTransaction,
)

Transaction = get_model_from_string("TRANSACTION")
Account = get_model_from_string("ACCOUNT")


@receiver(post_save, sender=InstallmentDownPayment)
def add_down_payment_transactions(instance, created, **kwargs):
    if created:
        transaction = Transaction()
        transaction.save_transaction_with_ledgers(
            title=f"Downpayment entry creation for {instance.subscription}",
            accounts=[
                {
                    "amount": instance.amount,
                    "account": Account.objects.get(name="installment receivable"),
                    "entry": "Dr",
                },
                {
                    "amount": instance.amount,
                    "account": Account.objects.get(name="installment revenue"),
                    "entry": "Cr",
                },
            ],
        )
        InstallmentTransaction.objects.create(
            subscription=instance.subscription,
            amount=instance.amount,
            action="ar",
            transaction=transaction,
        )


@receiver(post_save, sender=InstallmentDueExtension)
def update_subscription_status_on_extension(instance, created, **kwargs):
    if created:
        instance.subscription.payment_status = "ex"
        instance.subscription.save()


@receiver(post_save, sender=InstallmentDueExtension)
def update_extension_active_status(sender, instance, created, **kwargs):
    if created:
        sender.objects.exclude(pk=instance.id).update(is_last=False)
