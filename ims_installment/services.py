import datetime

from .models import InstallmentSubscription


def daily_installment_updation_operation():
    status = "su"
    log_entry = None
    messages = []
    due_counter = 0
    overdues_counter = 0

    installment_subscriptions = InstallmentSubscription.objects.filter(
        subscription_status="ac",
    )
    for installment_subscription in installment_subscriptions:
        try:
            increment_installment(installment_subscription)
        except Exception as error:
            messages.append(
                f"ERROR: Operation failed at subscription: {installment_subscription}"
                + "\n"
                + str(error)
            )
            status = "fa"
        else:
            due_counter += 1
        due_date = installment_subscription.get_current_due_data()[0]
        if due_date and due_date < datetime.date.today():
            installment_subscription.payment_status = "od"
            installment_subscription.save()
            overdues_counter += 1

    messages.append(
        """{} new dues.
        {} new overdues.
        """.format(
            due_counter,
            overdues_counter,
        )
    )
    log_entry = "\n".join(messages)
    return status, log_entry


def increment_installment(installment_subscription):
    if installment_subscription.is_due():
        installment_subscription.add_next_installment()
        installment_subscription.pay()
        if installment_subscription.is_due():
            increment_installment(installment_subscription)
