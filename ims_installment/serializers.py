from ims_base.serializers import BaseModelSerializer
from ims_operation.serializers import OperationBaseSerializer
from rest_framework import serializers
from reusable_models import get_model_from_string

from .models import InstallmentPercentageBreakup, InstallmentScheme
from .services import daily_installment_updation_operation

Operation = get_model_from_string("OPERATION")
OperationLog = get_model_from_string("OPERATION_LOG")


class InstallmentSchemeSerializer(BaseModelSerializer):
    periodic_installments = serializers.JSONField(write_only=True)

    class Meta(BaseModelSerializer.Meta):
        model = InstallmentScheme
        extra_meta = {
            "periodic_installments": {
                "native_type": "ArrayField",
            }
        }

    def validate_periodic_installments(self, value):
        array_sum = sum([int(item) for item in value])
        if array_sum != 100:
            raise serializers.ValidationError(
                "Sum of individual percentages is not 100"
            )
        return value

    def save_periodic_installments(self, periodic_installments, installment_scheme):
        number = 1
        for periodic_installment in periodic_installments:
            is_last = False
            if number == len(periodic_installments):
                is_last = True

            InstallmentPercentageBreakup.objects.create(
                scheme=installment_scheme,
                number=number,
                percent=periodic_installment,
                is_last=is_last,
            )
            number += 1

    def create(self, validated_data):
        periodic_installments = validated_data.pop("periodic_installments", [])
        installment_scheme = super().create(validated_data)
        self.save_periodic_installments(periodic_installments, installment_scheme)
        return installment_scheme

    def update(self, instance, validated_data):
        periodic_installments = validated_data.pop("periodic_installments", [])
        installment_scheme = super().update(instance, validated_data)
        InstallmentPercentageBreakup.objects.filter(scheme=installment_scheme).delete()
        self.save_periodic_installments(periodic_installments, installment_scheme)
        return installment_scheme

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data["periodic_installments"] = InstallmentPercentageBreakup.objects.filter(
            scheme=instance
        ).values_list("percent", flat=True)
        return data


class InstallmentsUpdateOperationSerializer(OperationBaseSerializer):
    class Meta(OperationBaseSerializer.Meta):
        pass

    def run_operation(self):
        status, log_entry = daily_installment_updation_operation()
        return status, log_entry
