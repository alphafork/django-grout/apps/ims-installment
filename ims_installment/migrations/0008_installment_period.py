# Generated by Django 4.1.7 on 2024-02-02 12:05

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("ims_installment", "0006_update_due_extension"),
    ]

    operations = [
        migrations.AddField(
            model_name="installment",
            name="period_from_month",
            field=models.PositiveSmallIntegerField(
                default=1,
                validators=[
                    django.core.validators.MinValueValidator(1),
                    django.core.validators.MaxValueValidator(12),
                ],
            ),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name="installment",
            name="period_from_year",
            field=models.PositiveSmallIntegerField(
                default=1,
                validators=[
                    django.core.validators.MinValueValidator(2000),
                    django.core.validators.MaxValueValidator(3000),
                ],
            ),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name="installment",
            name="period_to_month",
            field=models.PositiveSmallIntegerField(
                default=1,
                validators=[
                    django.core.validators.MinValueValidator(1),
                    django.core.validators.MaxValueValidator(12),
                ],
            ),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name="installment",
            name="period_to_year",
            field=models.PositiveSmallIntegerField(
                default=1,
                validators=[
                    django.core.validators.MinValueValidator(2000),
                    django.core.validators.MaxValueValidator(3000),
                ],
            ),
            preserve_default=False,
        ),
    ]
