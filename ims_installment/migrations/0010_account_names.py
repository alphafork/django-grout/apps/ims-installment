from django.db import migrations


def add_installment_accounts(apps, schema_editor):
    Account = apps.get_model("ims_accounting", "Account")
    AccountCategory = apps.get_model("ims_accounting", "AccountCategory")
    AccountType = apps.get_model("ims_accounting", "AccountType")
    liability = AccountType.objects.get(name="liability")
    asset = AccountType.objects.get(name="asset")
    expense = AccountCategory.objects.get(name="expense")
    advance_received = AccountCategory.objects.get_or_create(
        name="advance received",
        account_type=liability,
    )[0]
    doubtfull_account_allowance = AccountCategory.objects.get_or_create(
        name="allowance for doubtfull accounts",
        account_type=asset,
    )[0]
    accounts_receivable = AccountCategory.objects.get(name="accounts receivable")
    income = AccountCategory.objects.get(name="income")
    Account.objects.get_or_create(name="installment revenue", account_category=income)
    Account.objects.get_or_create(
        name="installment wallet", account_category=advance_received
    )
    Account.objects.get_or_create(
        name="installment receivable", account_category=accounts_receivable
    )
    Account.objects.get_or_create(name="bad debt expense", account_category=expense)
    AccountCategory.objects.get_or_create(
        name="installment write-off",
        account_category=doubtfull_account_allowance,
    )[0]


class Migration(migrations.Migration):
    dependencies = [
        ("ims_installment", "0009_update_transaction_status"),
    ]

    operations = [
        migrations.RunPython(
            add_installment_accounts, reverse_code=migrations.RunPython.noop
        ),
    ]
