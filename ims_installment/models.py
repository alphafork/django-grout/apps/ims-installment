import datetime

from django.contrib.auth import get_user_model
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from ims_base.models import AbstractBaseDetail, AbstractLog
from reusable_models import get_model_from_string

User = get_user_model()
Transaction = get_model_from_string("TRANSACTION")
Account = get_model_from_string("ACCOUNT")


class InstallmentScheme(AbstractBaseDetail):
    def installment_percentages(self):
        return InstallmentPercentageBreakup.objects.filter(scheme=self)

    def get_installment(self, amount, installment_no):
        breakup = self.installmentpercentagebreakup_set.filter(number=installment_no)
        if breakup:
            return breakup.get(), (breakup.get().percent * amount) / 100


class InstallmentPercentageBreakup(AbstractLog):
    scheme = models.ForeignKey(InstallmentScheme, on_delete=models.CASCADE)
    number = models.PositiveSmallIntegerField()
    percent = models.PositiveSmallIntegerField()
    is_last = models.BooleanField(default=False)

    def __str__(self):
        return self.scheme.name + " | " + str(self.number)


class InstallmentSubscription(AbstractLog):
    SUBSCRIPTION_STATUS = [
        ("ns", "not started"),
        ("ac", "active"),
        ("pu", "paused"),
        ("mi", "migrated"),
        ("ur", "unrecoverable"),
        ("cl", "closed"),
    ]
    PAYMENT_STATUS = [
        ("du", "due"),
        ("pa", "paid"),
        ("ex", "extension"),
        ("od", "overdue"),
    ]
    amount = models.PositiveIntegerField()
    start_date = models.DateField(null=True, blank=True)
    installment_day = models.PositiveSmallIntegerField(
        choices=[(i, i) for i in range(32)],
        null=True,
        blank=True,
    )
    scheme = models.ForeignKey(
        InstallmentScheme, on_delete=models.CASCADE, null=True, blank=True
    )
    subscription_status = models.CharField(
        max_length=2, choices=SUBSCRIPTION_STATUS, default="ns"
    )
    payment_status = models.CharField(
        max_length=2, choices=PAYMENT_STATUS, default="du"
    )

    def __str__(self):
        return str(self.amount) + " | " + self.scheme.__str__()

    def pay(self, wallet_payment_amount=0):
        installment_wallet = getattr(self, "installmentwallet", None)
        if not installment_wallet:
            installment_wallet = InstallmentWallet.objects.create(
                subscription=self,
                amount=wallet_payment_amount,
            )
        elif wallet_payment_amount > 0:
            installment_wallet.amount += wallet_payment_amount
            installment_wallet.save()
        if installment_wallet.amount == 0:
            return self
        initial_wallet_balance = wallet_balance = installment_wallet.amount
        down_payment = getattr(self, "installmentdownpayment", None)
        is_paid_full = True
        if down_payment and down_payment.balance_amount:
            if down_payment.balance_amount < wallet_balance:
                wallet_balance -= down_payment.balance_amount
                down_payment.balance_amount = 0
                is_paid_full = True
            else:
                is_paid_full = (
                    down_payment.balance_amount == wallet_balance
                ) & is_paid_full
                down_payment.balance_amount -= wallet_balance
                wallet_balance = 0
            down_payment.save()

        installments = self.installment_set.filter(balance_amount__gt=0)
        is_last = False
        for installment in installments:
            if installment.balance_amount < wallet_balance:
                wallet_balance -= installment.balance_amount
                installment.balance_amount = 0
                is_paid_full = True
            else:
                is_paid_full = (
                    installment.balance_amount == wallet_balance
                ) & is_paid_full
                installment.balance_amount -= wallet_balance
                wallet_balance = 0
            installment.save()
            is_last = installment.is_last()
        if is_paid_full:
            self.payment_status = "pa"
            if not self.scheme or is_last:
                self.subscription_status = "cl"
        self.save()

        cr_account = Account.objects.get(name="installment receivable")
        dr_account = Account.objects.get(name="installment wallet")
        transaction_amount = initial_wallet_balance - wallet_balance
        self.installmentwallet.amount = wallet_balance
        self.installmentwallet.save()
        accounts = [
            {"account": cr_account, "entry": "Cr", "amount": transaction_amount},
            {"account": dr_account, "entry": "Dr", "amount": transaction_amount},
        ]
        balance_updation_transaction = Transaction()
        balance_updation_transaction.save_transaction_with_ledgers(
            title=f"{self} wallet to subscription",
            accounts=accounts,
        )
        InstallmentTransaction.objects.create(
            subscription=self,
            amount=transaction_amount,
            action="bu",
            transaction=balance_updation_transaction,
        )
        return self

    def close_subscription(self):
        self.clear_wallet()
        cr_account = Account.objects.get(name="installment write-off")
        dr_account = Account.objects.get(name="bad debt expense")
        transaction_amount = self.current_payable()
        accounts = [
            {"account": cr_account, "entry": "Cr", "amount": transaction_amount},
            {"account": dr_account, "entry": "Dr", "amount": transaction_amount},
        ]
        write_off_transaction = Transaction()
        write_off_transaction.save_transaction_with_ledgers(
            title=f"{self} write-off",
            accounts=accounts,
        )
        InstallmentTransaction.objects.create(
            subscription=self,
            amount=transaction_amount,
            action="wo",
            transaction=write_off_transaction,
        )
        self.subscription_status = "cl"
        self.save()
        return self

    def clear_wallet(self):
        current_balance = self.current_balance()
        installment_wallet = getattr(self, "installmentwallet", None)
        wallet_balance = installment_wallet.amount if installment_wallet else 0
        if wallet_balance == 0:
            return self
        early_installment_dr_account = Account.objects.get(
            name="installment receivable"
        )
        early_installment_cr_account = Account.objects.get(name="installment revenue")
        wallet_payment_dr_account = Account.objects.get(name="installment wallet")
        wallet_payment_cr_account = Account.objects.get(name="installment receivable")
        if wallet_balance <= current_balance:
            transaction_amount = wallet_balance
            wallet_balance = 0
        else:
            transaction_amount = current_balance
            wallet_balance -= current_balance
        self.installmentwallet.amount = wallet_balance
        self.installmentwallet.save()
        accounts = [
            {
                "account": early_installment_cr_account,
                "entry": "Cr",
                "amount": transaction_amount,
            },
            {
                "account": early_installment_dr_account,
                "entry": "Dr",
                "amount": transaction_amount,
            },
        ]
        wallet_payment_transaction = Transaction()
        wallet_payment_transaction.save_transaction_with_ledgers(
            title=f"{self} early installment",
            accounts=accounts,
        )
        InstallmentTransaction.objects.create(
            subscription=self,
            amount=transaction_amount,
            action="ar",
            transaction=wallet_payment_transaction,
        )
        accounts = [
            {
                "account": wallet_payment_cr_account,
                "entry": "Cr",
                "amount": transaction_amount,
            },
            {
                "account": wallet_payment_dr_account,
                "entry": "Dr",
                "amount": transaction_amount,
            },
        ]
        wallet_payment_transaction = Transaction()
        wallet_payment_transaction.save_transaction_with_ledgers(
            title=f"{self} wallet to subscription",
            accounts=accounts,
        )
        InstallmentTransaction.objects.create(
            subscription=self,
            amount=transaction_amount,
            action="bu",
            transaction=wallet_payment_transaction,
        )
        return self

    def return_wallet_payment(self, return_amount=None):
        installment_wallet = getattr(self, "installmentwallet", None)
        wallet_balance = installment_wallet.amount if installment_wallet else 0
        return_amount = return_amount or wallet_balance
        if return_amount > wallet_balance:
            raise ValueError("Given amount is higher than wallet balance.")
        cr_account = Account.objects.get(name="installment wallet")
        dr_account = Account.objects.get(name="accounts payable")
        self.installmentwallet.amount = wallet_balance - return_amount
        self.installmentwallet.save()
        accounts = [
            {"account": cr_account, "entry": "Cr", "amount": return_amount},
            {"account": dr_account, "entry": "Dr", "amount": return_amount},
        ]
        balance_updation_transaction = Transaction()
        balance_updation_transaction.save_transaction_with_ledgers(
            title=f"Return {self} wallet payment",
            accounts=accounts,
        )
        InstallmentTransaction.objects.create(
            subscription=self,
            amount=return_amount,
            action="rw",
            transaction=balance_updation_transaction,
        )
        return self

    def current_payable(self):
        down_payment = getattr(self, "installmentdownpayment", None)
        installments = self.installment_set.all()
        installment_balances = installments.filter(balance_amount__gt=0)
        total_balance = sum(
            [installment.balance_amount for installment in installment_balances]
        )
        if down_payment:
            total_balance += down_payment.balance_amount
        return total_balance

    def current_balance(self):
        down_payment = getattr(self, "installmentdownpayment", None)
        installments = self.installment_set.all()
        total_payable = sum([installment.amount for installment in installments])
        if down_payment:
            total_payable += down_payment.amount
        total_balance = self.current_payable()
        return self.amount - (total_payable - total_balance)

    def is_due(self):
        today_date = datetime.date.today()
        due_day = self.installment_day or self.start_date.day
        installments = self.installment_set.all()
        if self.subscription_status != "ac":
            return False
        if not self.scheme and not hasattr(self, "installmentdownpayment"):
            return False
        if installments:
            last_installment = installments.order_by("number__number").last()
            if last_installment.is_last():
                return False
            installment_period_value = (
                last_installment.period_to_year * 100 + last_installment.period_to_month
            )
            if int(today_date.strftime("%Y%m")) > installment_period_value:
                return True
            elif int(today_date.strftime("%Y%m")) == installment_period_value:
                return due_day < today_date.day
        else:
            return due_day < today_date.day

    def add_next_installment(self):
        installments = self.installment_set.all()
        if not self.scheme:
            return None
        if installments:
            last_installment = installments.order_by("number__number").last()
            if last_installment.is_last():
                return None
            next_installment_no = last_installment.number.number + 1
            last_period_to_month = last_installment.period_to_month
            last_period_to_year = last_installment.period_to_year
        else:
            next_installment_no = 1
            last_period_to_month = self.start_date.month
            last_period_to_year = self.start_date.year

        next_installment_period_from_month = last_period_to_month
        next_installment_period_from_year = last_period_to_year
        if last_period_to_month == 12:
            next_installment_period_to_month = 1
            next_installment_period_to_year = last_period_to_year + 1
        else:
            next_installment_period_to_month = last_period_to_month + 1
            next_installment_period_to_year = last_period_to_year

        total_installment_amount = self.amount
        if hasattr(self, "installmentdownpayment"):
            total_installment_amount -= self.installmentdownpayment.amount
        installment_no, installment_amount = self.scheme.get_installment(
            total_installment_amount,
            next_installment_no,
        )
        next_due_day = self.installment_day or self.start_date.day
        next_due_date = datetime.date(
            next_installment_period_to_year,
            next_installment_period_to_month,
            next_due_day,
        )
        next_installment = Installment.objects.create(
            number=installment_no,
            subscription=self,
            amount=installment_amount,
            balance_amount=installment_amount,
            due_date=next_due_date,
            period_from_month=next_installment_period_from_month,
            period_from_year=next_installment_period_from_year,
            period_to_month=next_installment_period_to_month,
            period_to_year=next_installment_period_to_year,
        )
        if self.payment_status == "pa":
            self.payment_status = "du"
            self.save()
        transaction = Transaction()
        transaction.save_transaction_with_ledgers(
            title=f"{installment_no} Installment entry creation for {self}",
            accounts=[
                {
                    "amount": installment_amount,
                    "account": Account.objects.get(name="installment receivable"),
                    "entry": "Dr",
                },
                {
                    "amount": installment_amount,
                    "account": Account.objects.get(name="installment revenue"),
                    "entry": "Cr",
                },
            ],
        )
        InstallmentTransaction.objects.create(
            subscription=self,
            amount=installment_amount,
            action="ar",
            transaction=transaction,
        )
        return next_installment

    def get_current_due_data(self):
        current_payable = 0
        oldest_due_installment = (
            Installment.objects.filter(subscription=self, balance_amount__gt=0)
            .order_by("number__number")
            .first()
        )
        last_extension = InstallmentDueExtension.objects.filter(
            subscription=self,
            is_last=True,
        )
        installment_downpayment = getattr(self, "installmentdownpayment", None)

        due_dates = []
        if installment_downpayment and installment_downpayment.balance_amount > 0:
            current_payable += installment_downpayment.balance_amount
            due_dates.append(self.start_date)
        if oldest_due_installment:
            due_dates.append(oldest_due_installment.due_date)
        current_due_date = ""
        if len(due_dates) > 0:
            current_due_date = min(due_dates)
        if (
            current_due_date
            and last_extension
            and last_extension.get().extended_date > current_due_date
        ):
            current_due_date = last_extension.get().extended_date

        installment_balance_amount = Installment.objects.filter(
            subscription=self
        ).aggregate(models.Sum("balance_amount"))["balance_amount__sum"]
        if installment_balance_amount:
            current_payable += installment_balance_amount

        return current_due_date, current_payable

    def get_total_paid(self):
        transaction_amount_list = [
            transaction.amount
            for transaction in self.installmenttransaction_set.filter(action="wp")
        ]
        return sum(transaction_amount_list)

    def get_payment_timeline(self):
        payment_history = []
        total_amount_installments = self.amount
        installment_downpayment = getattr(self, "installmentdownpayment", None)
        if installment_downpayment:
            total_amount_installments -= installment_downpayment.amount
            payment_history.append(
                {
                    "item": "down payment",
                    "due_date": self.start_date,
                    "amount": installment_downpayment.amount,
                    "balance_amount": installment_downpayment.balance_amount,
                }
            )

        if self.scheme:
            installment_parts = self.scheme.installment_percentages()
            cumulative_percentage = 0
            for installment_part in installment_parts:
                installment = Installment.objects.filter(
                    subscription=self,
                    number=installment_part,
                )
                if installment or self.subscription_status == "ac":
                    cumulative_percentage += installment_part.percent
                    installment_dict = {
                        "item": f"installment {installment_part.number}",
                        "amount": round(
                            total_amount_installments * installment_part.percent / 100
                        ),
                        "percentage": f"{installment_part.percent}%",
                        "cumulative_percentage": f"{cumulative_percentage}%",
                    }
                    if installment:
                        installment = installment.get()
                        installment_dict.update(
                            {
                                "due_date": installment.due_date,
                                "balance_amount": installment.balance_amount,
                            }
                        )

                    payment_history.append(installment_dict)
        return payment_history


class InstallmentDownPayment(AbstractLog):
    subscription = models.OneToOneField(
        InstallmentSubscription, on_delete=models.CASCADE
    )
    amount = models.PositiveIntegerField()
    balance_amount = models.PositiveIntegerField()

    def __str__(self):
        return self.subscription.__str__()


class InstallmentWallet(AbstractLog):
    subscription = models.OneToOneField(
        InstallmentSubscription, on_delete=models.CASCADE
    )
    amount = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return self.subscription.__str__()


class Installment(AbstractLog):
    number = models.ForeignKey(InstallmentPercentageBreakup, on_delete=models.CASCADE)
    subscription = models.ForeignKey(InstallmentSubscription, on_delete=models.CASCADE)
    amount = models.IntegerField(null=True, blank=True)
    balance_amount = models.IntegerField(null=True, blank=True)
    due_date = models.DateField()
    period_from_month = models.PositiveSmallIntegerField(
        validators=[MinValueValidator(1), MaxValueValidator(12)]
    )
    period_from_year = models.PositiveSmallIntegerField(
        validators=[MinValueValidator(2000), MaxValueValidator(3000)]
    )
    period_to_month = models.PositiveSmallIntegerField(
        validators=[MinValueValidator(1), MaxValueValidator(12)]
    )
    period_to_year = models.PositiveSmallIntegerField(
        validators=[MinValueValidator(2000), MaxValueValidator(3000)]
    )

    def __str__(self):
        return self.subscription.__str__()

    def is_last(self):
        return self.number.is_last


class InstallmentDueExtension(AbstractLog):
    subscription = models.ForeignKey(InstallmentSubscription, on_delete=models.CASCADE)
    extended_date = models.DateField()
    remarks = models.TextField(blank=True)
    is_last = models.BooleanField(default=False)

    def __str__(self):
        return self.extended_date.strftime("%d-%m-%Y")


class InstallmentTransaction(AbstractLog):
    ACTION = [
        ("ar", "add recievable"),
        ("wp", "wallet payment"),
        ("bu", "balance updation"),
        ("su", "set unrecoverable"),
        ("ru", "reverse unrecoverable"),
        ("wo", "write off"),
        ("rw", "return wallet payment"),
    ]
    subscription = models.ForeignKey(InstallmentSubscription, on_delete=models.CASCADE)
    transaction = models.ForeignKey(Transaction, on_delete=models.CASCADE)
    amount = models.PositiveIntegerField(null=True, blank=True)
    action = models.CharField(max_length=2, choices=ACTION)

    def __str__(self):
        return self.subscription.__str__()
